package newaz.truecaller.event;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;



public  class TruecallerBroadcastRequest {

    private static final String ACTION_DONE= "newaz.truecaller.event.TruecallerBroadcastRequest.DONE";
    public static final String DATA= "newaz.truecaller.event.TruecallerBroadcastRequest.DATA";
    public static final String REQUEST_TYPE= "newaz.truecaller.event.TruecallerBroadcastRequest.REQUEST_TYPE";

    private TruecallerBroadcastRequest() {
    }

    public  static IntentFilter buildIntentFilter(){
        return new IntentFilter(TruecallerBroadcastRequest.ACTION_DONE);
    }


    public static void broadcast(final Context context, final Bundle b) {
        final Intent intent = new Intent(ACTION_DONE);
        intent.putExtras(b);
        TruecallerLocalBroadcastReceiver.sendBroadcast(context, intent);
    }
}
