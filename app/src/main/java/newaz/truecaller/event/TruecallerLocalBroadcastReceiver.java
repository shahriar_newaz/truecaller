package newaz.truecaller.event;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

public abstract class TruecallerLocalBroadcastReceiver extends BroadcastReceiver {


    public static void sendBroadcast(final Context context, final Intent intent){
        final LocalBroadcastManager manager = LocalBroadcastManager.getInstance(context);
        manager.sendBroadcast(intent);
    }


    public static void registerLocalBroadcastReceiver(final Context context, final BroadcastReceiver receiver,
                                             final IntentFilter filter) {
        final LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
        localBroadcastManager.registerReceiver(receiver, filter);
    }

    public static void unregisterLocalBroadcastReceiver(final Context context, final BroadcastReceiver receiver) {
        final LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
        localBroadcastManager.unregisterReceiver(receiver);
    }

}
