package newaz.truecaller.utils;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpManager {

    private static final int READ_TIMEOUT = 10000;
    private static final int CONNECT_TIMEOUT = 15000;

    public static String get(String httpUrl){
        BufferedReader bufferedReader=null;
        String response = "";
        try {
            URL url = new URL(httpUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url
                    .openConnection();
            urlConnection.setReadTimeout(READ_TIMEOUT);
            urlConnection.setConnectTimeout(CONNECT_TIMEOUT);
            urlConnection.setRequestMethod("GET");
            bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            if (bufferedReader != null) {
                StringBuilder builder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    builder.append(line);
                }
                response = builder.toString().trim();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
               bufferedReader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return response;

    }

}
