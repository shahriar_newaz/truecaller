package newaz.truecaller.utils;


public interface ContentManager {

    String getUrl();
    void handleContent(String response);

}
