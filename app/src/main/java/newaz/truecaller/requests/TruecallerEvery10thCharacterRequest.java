package newaz.truecaller.requests;


import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

import newaz.truecaller.utils.ContentManager;
import newaz.truecaller.TruecallerApplication;
import newaz.truecaller.event.TruecallerBroadcastRequest;

public class TruecallerEvery10thCharacterRequest extends TruecallerRequest implements ContentManager {



    public TruecallerEvery10thCharacterRequest(String url) {
        super(url);
        downloadRunnable = new ContentDownloadRunnable(this);
    }

    private TruecallerEvery10thCharacterRequest(){
        super();
    }


    @Override
    public String getUrl() {
        return url;
    }


    @Override
    public void handleContent(String response) {
        Log.d(TruecallerEvery10thCharacterRequest.class.getName(), "resp:" + response);

        ArrayList<Character> characterArrayList = new ArrayList<>();

        int index=9;
        while(index<response.length())
        {
            characterArrayList.add(response.charAt(index));
            index+=10;
        }

        Bundle responseBundle = new Bundle();
        responseBundle.putSerializable(TruecallerBroadcastRequest.DATA, characterArrayList);
        responseBundle.putSerializable(TruecallerBroadcastRequest.REQUEST_TYPE, RequestType.FIND_EVERY_10TH_CHARACTER_REQUEST);
        TruecallerBroadcastRequest.broadcast(TruecallerApplication.getAppContext(), responseBundle);


    }



    public Runnable getRunnable() {
        return downloadRunnable;
    }

}
