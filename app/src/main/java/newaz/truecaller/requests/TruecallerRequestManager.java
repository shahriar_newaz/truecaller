package newaz.truecaller.requests;


import android.os.Handler;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TruecallerRequestManager {

    private final ThreadPoolExecutor threadPoolExecutor;
    private static TruecallerRequestManager requestManager = null;
    private final BlockingQueue<Runnable> runnables;

    static{
        requestManager = new TruecallerRequestManager();
    }

    public static TruecallerRequestManager getInstance() {
        return requestManager;
    }


    private TruecallerRequestManager(){
        runnables = new ArrayBlockingQueue(1024);

        threadPoolExecutor = new ThreadPoolExecutor(4, 8,
                2, TimeUnit.SECONDS, runnables);

    }


    public static void startRequest(Runnable runnable){
        requestManager.threadPoolExecutor.execute(runnable);
    }


}
