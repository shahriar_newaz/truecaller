package newaz.truecaller.requests;


public enum RequestType {
    FIND_10TH_CHARACTER_REQUEST,
    FIND_EVERY_10TH_CHARACTER_REQUEST,
    WORD_COUNTER_REQUEST;
}
