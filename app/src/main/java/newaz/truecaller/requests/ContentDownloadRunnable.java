package newaz.truecaller.requests;


import android.util.Log;

import newaz.truecaller.utils.ContentManager;
import newaz.truecaller.utils.HttpManager;

public class ContentDownloadRunnable implements Runnable {

    private final ContentManager contentHolder;

    public ContentDownloadRunnable(ContentManager holder) {
        this.contentHolder = holder;
    }

    @Override
    public void run() {
        String response =  HttpManager.get(contentHolder.getUrl());
        contentHolder.handleContent(response);
        Log.d(ContentDownloadRunnable.class.getName(), "resp:" + response);

    }
 }
