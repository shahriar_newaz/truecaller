package newaz.truecaller.requests;


import android.os.Bundle;
import android.util.Log;

import newaz.truecaller.utils.ContentManager;
import newaz.truecaller.TruecallerApplication;
import newaz.truecaller.event.TruecallerBroadcastRequest;

public class Truecaller10thCharacterRequest extends TruecallerRequest implements ContentManager {


    public Truecaller10thCharacterRequest(String url) {
        super(url);
        downloadRunnable = new ContentDownloadRunnable(this);
    }


    private Truecaller10thCharacterRequest(){
        super();
    }


    @Override
    public String getUrl() {
        return url;
    }


    @Override
    public void handleContent(String response) {
        Bundle responseBundle = new Bundle();
        responseBundle.putChar(TruecallerBroadcastRequest.DATA,response.charAt(9));
        responseBundle.putSerializable(TruecallerBroadcastRequest.REQUEST_TYPE, RequestType.FIND_10TH_CHARACTER_REQUEST);
        TruecallerBroadcastRequest.broadcast(TruecallerApplication.getAppContext(), responseBundle);

    }


    public Runnable getRunnable() {
        return downloadRunnable;
    }

}
