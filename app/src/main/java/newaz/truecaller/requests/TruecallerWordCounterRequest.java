package newaz.truecaller.requests;


import android.os.Bundle;

import java.util.HashMap;

import newaz.truecaller.utils.ContentManager;
import newaz.truecaller.TruecallerApplication;
import newaz.truecaller.event.TruecallerBroadcastRequest;

public class TruecallerWordCounterRequest extends TruecallerRequest implements ContentManager {

    public TruecallerWordCounterRequest(String url) {
        super(url);
        downloadRunnable = new ContentDownloadRunnable(this);
    }

    private TruecallerWordCounterRequest(){
        super();
    }


    @Override
    public String getUrl() {
        return url;
    }


    @Override
    public void handleContent(String response) {
        HashMap<String,Integer> wordFrequencies= new HashMap<>();

        String[] splitValues =  response.toLowerCase().split("\\s+");
        for(String word: splitValues){
            if(!wordFrequencies.containsKey(word))
                wordFrequencies.put(word,1);
            else {
                int wordCount=wordFrequencies.get(word);
                wordFrequencies.put(word,wordCount+1);
            }
        }

        Bundle responseBundle = new Bundle();
        responseBundle.putSerializable(TruecallerBroadcastRequest.DATA, wordFrequencies);
        responseBundle.putSerializable(TruecallerBroadcastRequest.REQUEST_TYPE, RequestType.WORD_COUNTER_REQUEST);
        TruecallerBroadcastRequest.broadcast(TruecallerApplication.getAppContext(), responseBundle);

    }



    public Runnable getRunnable() {
        return downloadRunnable;
    }

}
