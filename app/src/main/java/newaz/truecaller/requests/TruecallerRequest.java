package newaz.truecaller.requests;


import org.apache.commons.lang3.Validate;

import newaz.truecaller.utils.Constants;

public abstract class TruecallerRequest  {

    public ContentDownloadRunnable downloadRunnable;


    //default is support page for this demo only if no url is specified
    protected String url  = Constants.TRUECALLER_SUPPORT_PAGE;
    protected TruecallerRequest(String url){
        Validate.notNull(url, "url can't be null");
        Validate.notEmpty(url, "url can't be empty");
         this.url = url;

    }

    public TruecallerRequest() {

    }
}
