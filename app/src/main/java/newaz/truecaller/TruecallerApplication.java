package newaz.truecaller;


import android.app.Application;

public class TruecallerApplication extends Application {

    private static TruecallerApplication app;

    public static final TruecallerApplication getAppContext() {
        return TruecallerApplication.app;
    }

    @Override
    public void onCreate() {
        app = this;
        super.onCreate();
    }
}
