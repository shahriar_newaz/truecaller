package newaz.truecaller.ui;


import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import newaz.truecaller.R;
import newaz.truecaller.TruecallerApplication;
import newaz.truecaller.requests.RequestType;
import newaz.truecaller.event.TruecallerBroadcastRequest;
import newaz.truecaller.event.TruecallerLocalBroadcastReceiver;
import newaz.truecaller.requests.Truecaller10thCharacterRequest;
import newaz.truecaller.requests.TruecallerEvery10thCharacterRequest;
import newaz.truecaller.requests.TruecallerRequestManager;
import newaz.truecaller.requests.TruecallerWordCounterRequest;
import newaz.truecaller.utils.Constants;
import newaz.truecaller.utils.TruecallerUtil;

public class TruecallerRequestFragment extends Fragment {

    public static final String TAG = TruecallerRequestFragment.class.getName();

    private FindCharactersBC findThenthCharacterBC = new FindCharactersBC();
    private HashMap<String, Integer> wordMap = new HashMap<>();

    public static TruecallerRequestFragment newInstance() {
        TruecallerRequestFragment fragment = new TruecallerRequestFragment();
         return fragment;
    }

    public TruecallerRequestFragment() {
    }


    @Override
    public void onResume() {
        super.onResume();
        findThenthCharacterBC.register();
    }


    @Override
    public void onPause() {
        super.onPause();
        findThenthCharacterBC.unregister();
    }


    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {

        View fragmentView = inflater.inflate(R.layout.fragment_truecaller_request, container, false);
        Button btnRquest = (Button) fragmentView.findViewById(R.id.btnRequest);
        btnRquest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAllTextViews();
                if(TruecallerUtil.isInternetAvailable(getActivity())) {
                  try {
                      Truecaller10thCharacterRequest truecaller10thCharacterRequest =
                              new Truecaller10thCharacterRequest(Constants.TRUECALLER_SUPPORT_PAGE);
                      TruecallerRequestManager.startRequest(truecaller10thCharacterRequest.getRunnable());

                      TruecallerEvery10thCharacterRequest truecallerEvery10thCharacterRequest =
                              new TruecallerEvery10thCharacterRequest(Constants.TRUECALLER_SUPPORT_PAGE);
                      TruecallerRequestManager.startRequest(truecallerEvery10thCharacterRequest.getRunnable());

                      TruecallerWordCounterRequest truecallerWordCounterRequest =
                              new TruecallerWordCounterRequest(Constants.TRUECALLER_SUPPORT_PAGE);
                      TruecallerRequestManager.startRequest(truecallerWordCounterRequest.getRunnable());
                  }catch (IllegalArgumentException iae){
                      Log.d(TAG,"URL must not be null or empty");
                  }
                }

            }
        });
        return fragmentView;

    }

    private void clearAllTextViews() {
        View fragmentView = getView();
        if(fragmentView != null) {

            final TextView tvTenthChar = (TextView) fragmentView.findViewById(R.id.tvTenthChar);
            if(tvTenthChar!=null){
                tvTenthChar.setText("");
            }

            final TextView tvEveryTenthChar = (TextView) fragmentView.findViewById(R.id.tvEveryTenthChar);
            if(tvEveryTenthChar!=null){
                tvEveryTenthChar.setText("");
            }

            final TextView tvWordCounter = (TextView) fragmentView.findViewById(R.id.tvWordCounter);
            if(tvWordCounter!=null){
                tvWordCounter.setText("");
            }

        }
    }


    private void updateTenthCharTextView(Character thenChar) {
        displayResult(R.id.tvTenthChar, thenChar);
    }


    private void updateEveryTenthCharTextView(ArrayList<Character> charList) {
           displayResult(R.id.tvEveryTenthChar, charList);
    }


    private void updateWordCounterTextView(HashMap<String, Integer> wordFrequencies) {
        displayResult(R.id.tvWordCounter, wordFrequencies);
    }



    private void displayResult(int textView, Object o) {
        View fragmentView = getView();
        if(fragmentView != null) {
            final TextView tv = (TextView) fragmentView.findViewById(textView);
            tv.setText(o.toString());

        }
    }


    public class FindCharactersBC extends TruecallerLocalBroadcastReceiver {

        void register() {
            final IntentFilter filter = TruecallerBroadcastRequest.buildIntentFilter();
            registerLocalBroadcastReceiver(TruecallerApplication.getAppContext(), this, filter);
        }

        void unregister() {
            unregisterLocalBroadcastReceiver(TruecallerApplication.getAppContext(), this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle responseBundle = intent.getExtras();
            if(responseBundle!=null){
                RequestType requestType = (RequestType)
                        responseBundle.getSerializable(TruecallerBroadcastRequest.REQUEST_TYPE);
                switch (requestType) {
                    case FIND_10TH_CHARACTER_REQUEST:
                        updateTenthCharTextView(responseBundle.getChar(TruecallerBroadcastRequest.DATA));
                        break;

                    case FIND_EVERY_10TH_CHARACTER_REQUEST:
                        updateEveryTenthCharTextView((ArrayList<Character>)
                                responseBundle.getSerializable(TruecallerBroadcastRequest.DATA));
                        break;

                    case WORD_COUNTER_REQUEST:
                        HashMap<String, Integer> wordMap = (HashMap<String, Integer>)
                                responseBundle.getSerializable(TruecallerBroadcastRequest.DATA);
                        setWordMap(wordMap);
                        updateWordCounterTextView(wordMap);
                        break;
                }
            }

        }
    }

    private void setWordMap(HashMap<String, Integer> wordMap) {
        this.wordMap = wordMap;
    }

    public void getWordCountOf(String wordToFind) {
        // user can search for how many times a word occurs using  this.wordMap
    }
}
