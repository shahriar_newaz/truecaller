package newaz.truecaller.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import newaz.truecaller.R;
import newaz.truecaller.requests.Truecaller10thCharacterRequest;
import newaz.truecaller.requests.TruecallerRequest;


public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
            TruecallerRequestFragment truecallerRequestFragment = TruecallerRequestFragment.newInstance();
            truecallerRequestFragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, truecallerRequestFragment).commit();

        }
    }

}
